# Links úteis

- Documentação React Native: https://facebook.github.io/react-native/docs/getting-started.html
- Running on device: https://facebook.github.io/react-native/docs/running-on-device.html
- Debugging: https://facebook.github.io/react-native/docs/debugging.html#accessing-the-in-app-developer-menu
- Tutoriais: http://www.reactnativeexpress.com

# Downloads necessários

- Node: https://nodejs.org/dist/v8.11.1/node-v8.11.1-x64.msi
- Python 2: https://www.python.org/ftp/python/2.7.14/python-2.7.14.msi
- JDK 8: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
- Android Studio: https://developer.android.com/studio/index.html?hl=pt-br#win-bundle
- [MacOs]: Homebrew: https://brew.sh/index_pt-br

## Configurar variáveis de Usuário

- _JAVA_HOME: C:\Program Files\Java\jdk-_.\*.\*\*
- _ANDROID_HOME: C:\Users\%user%\AppData\Local\Android\Sdk_

## Instalar React Native CLI:

```
npm install -g react-native-cli
```

# Comandos

## Android

### (Windows Only) Para o app conectar no servidor de desenvolvimento via usb `(precisa ter o driver usb (adb) configurado)`

```
[npm/yarn] run adb-reverse
```

### Abrir menu de desenvolvimento do app

Sacudir o device ou

```
[npm/yarn] run android-menu
```

caso esteja utilizando um emulador android, pode-se usar o atalho `ctrl + M`

`[Windows]: precisa ter o driver usb (adb) configurado`
`[MacOs]: utilizando emulador android utilizar o atalho command + M`

### Limpar build

```
[npm/yarn] run android-clean
```

### Executar o app

```
react-native run-android
```

## IOS

### Abrir menu de desenvolvimento do app

```
Sacudir o device ou caso esteja utilizando emulador, pelo atalho command + D
```

### Executar o app

```
react-native run-ios
```

## Globais

### Instalar packages

```
[npm/yarn] install
```

### Linkar módulos nativos

```
react-native link
```

### Iniciar servidor

```
npm start
```

### Iniciar servidor limpando o cache

```
npm start -- --reset-cache
```

### Verificar se packages estão atualizados

```
npm-check
```

### Atualizar packages desatualizados

```
npm-check -u
```

### Rodar testes

```
npm run test
```

# Windows - Driver USB (ADB)

https://developer.android.com/studio/run/oem-usb?hl=pt-br

# Endpoints

http://cn04.lojasrenner.com.br:30050/api/parametros
http://cn04.lojasrenner.com.br:30050/api/autenticacao
http://cn04.lojasrenner.com.br:30050/api/lista
http://cn04.lojasrenner.com.br:30050/api/menu
http://cn04.lojasrenner.com.br:30050/api/busca
